<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\File;
use Illuminate\Support\Facades\DB;

class FileController extends Controller
{
    //
    public function storeFile(Request $request)
    {

        if ($request->hasFile('image')) {
            $data = $request->input('image');
            $photo = $request->file('image')->getClientOriginalName();
            $size = $request->file('image')->getSize();
            $destination = base_path() . '/public/uploads';
            $request->file('image')->move($destination, $photo);
            $file = new File;
            $file->name = $photo;
            $file->size = $size;
            // $path = realpath($request->file('image')->path());
            //$extension = $request->file('image')->getClientOriginalExtension();
            //$prefix = [$photo, $extension];
            //$output = join(".", $prefix);
            //$imagedata = file_get_contents('E:\AN4\DAW\tema3\backend\projCiumafai\public\uploads\$output');
            // alternatively specify an URL, if PHP settings allow
            //$base64 = base64_encode($imagedata);
            $path = "E:\AN4\DAW\tema3\backend\projCiumafai\public\uploads\avatar.png";
            $image = base64_encode(file_get_contents('E:\AN4\DAW\tema3\backend\projCiumafai\public\uploads\\' . $photo));
            DB::insert('insert into files (name,size,base64data) values (?, ?,?)', [$photo, $size, $image]);
            return "yes";
        }
    }
}
