import Enzyme from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import { shallow } from "enzyme";
import App from "./App";
import React from "react";
import { Switch, Route } from "react-router-dom";

Enzyme.configure({ adapter: new Adapter() });

const wrapper = shallow(<App />);

it("should contain a Switch component", () => {
  expect(wrapper.find(Switch)).toHaveLength(1);
});
it("should contain 13 Route components", () => {
  expect(wrapper.find(Route)).toHaveLength(13);
});
