import * as React from "react";
import { useTranslation } from "react-i18next";
import { Button } from "primereact/button";

function Language() {
  const { t, i18n } = useTranslation();

  return (
    <div style={{ width: "100%", margin: "auto" }}>
      <br />
      <div className="banner-text">
        <br></br>
        <div>
          <div className="content-section introduction">
            <div className="feature-intro">
              <h1>{t("language.1")} </h1>
            </div>
          </div>

          <div className="buttons">
            <Button
              label="Romana"
              className="p-button-secondary"
              onClick={() =>
                i18n.changeLanguage("ro") &
                localStorage.setItem("language", "ro")
              }
            />
            <br />
            <br />
            <Button
              label="English"
              className="p-button-secondary"
              onClick={() =>
                i18n.changeLanguage("en") &
                localStorage.setItem("language", "en")
              }
            />
          </div>
          <p></p>
        </div>
      </div>
    </div>
  );
}

export default Language;
