import * as React from "react";
import "../css/About.css";
import { Grid, Cell } from "react-mdl";
import { Button } from "primereact/components/button/Button";
import axios from "axios";
import { useTranslation } from "react-i18next";
import Column from "primereact/datatable";

import { InputText } from "primereact/inputtext";
import { DataTable } from "primereact/datatable";
function MyComponent() {
  const { t } = useTranslation();
  return <h1>{t("about.1")}</h1>;
}
function MyComponent2() {
  const { t } = useTranslation();
  return <h1>{t("about.2")}</h1>;
}
function MyComponent3() {
  const { t } = useTranslation();
  return <p>{t("about.3")}</p>;
}

class About extends React.Component {
  constructor() {
    super();
    this.state = {
      commModel: {
        value: "",
      },
      commuri: [],
      errorlogin: false,
    };
  }
  submit() {
    if (sessionStorage.length === 0) {
      this.setState({ errorlogin: true });
      console.log("erorarere");
    } else {
      //axios post cu valoare din comment
      console.log(this.state.commModel);
      axios
        .post("http://localhost:8000/api/postComment", this.state.commModel, {
          headers: { "Content-Type": "application/json" },
          withCredentials: true,
        })
        .then((response) => {
          console.log("com adaugat cu succes");
          window.location.reload("false");
        })
        .catch(() => {
          console.log("Eroare");
        });
    }
  }
  submit2 = () => {
    this.props.history.push(`/chart`);
  };
  componentDidMount() {
    axios
      .get("http://localhost:8000/api/getComment", {
        headers: { "Content-Type": "application/json" },
        withCredentials: true,
      })
      .then((response) => {
        this.setState({ commuri: response.data });
        console.log(this.state.commuri);
        console.log("succes aducere comms");
      })
      .catch(() => {
        console.log("Eroare");
      });
  }
  render() {
    var errorlogin = this.state.errorlogin;
    return (
      <div style={{ width: "100%", margin: "auto" }}>
        <Grid className="landing-grid">
          <Cell col={2}>
            <br />
            <div className="banner-text">
              <br />
              <MyComponent />
              <br />
            </div>
          </Cell>
          <br />
          <Cell col={2}>
            <div className="banner-text">
              <br />
              <hr />

              <MyComponent2 />

              <hr />

              <MyComponent3 />
            </div>
          </Cell>
          <div className="banner-text">
            <br />
            <h1>Comment Section</h1>
            <br></br>
            <span className="p-float-label">
              <InputText
                id="in"
                value={this.state.value}
                onChange={(event) => {
                  let commModel = this.state.commModel;
                  commModel.value = event.target.value;
                  this.setState({ commModel: commModel });
                }}
              />
              <br /> <br />
              <Button label="Add" onClick={() => this.submit()} />
              <br /> <br />
              <Button
                data-testid2="button"
                label="Chart Comments"
                onClick={() => this.submit2()}
              />
              {errorlogin === true ? <h1>Please login before!</h1> : null}
            </span>
            <br />
            <DataTable value={this.state.commuri}>
              <Column field="id_user" header="Id user" />
              <Column field="name" header="Name " />
              <Column field="comm" header="Comment" />
              <Column field="created_at" header="Posted at" />
            </DataTable>
            <br />
            <br />
            <br />
          </div>
        </Grid>
      </div>
    );
  }
}
export default About;
