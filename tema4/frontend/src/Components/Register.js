import * as React from "react";
import { InputText } from "primereact/components/inputtext/InputText";
import { Button } from "primereact/components/button/Button";
import { Message } from "primereact/components/message/Message";

import { UserService } from "./UserService";
import { NotificationManager } from "react-notifications";
import "../css/Register.css";
//import signup from '../img/avatar3.png';

export class Register extends React.Component {
  constructor(usersProps) {
    super(usersProps);

    this.state = {
      userModel: {
        name: "",
        email: "",
        password: "",
      },
      errors: {
        name: "",
        email: "",
        password: "",
      },
      isValid: false,
    };
  }

  submit() {
    let isValid = UserService.validateUser(
      this.state.userModel,
      this.state.errors
    );

    if (isValid) {
      UserService.addUser(this.state.userModel).then(
        () => {
          NotificationManager.success("Contul a fost creat !", "Felicitari !");
          this.setState({ isValid: true });
        },
        (error) => {
          NotificationManager.error(error.message);
          this.setState({ isValid: false });
        }
      );
    } else {
      this.setState({ isValid: false });
    }
  }

  render() {
    return (
      <React.Fragment>
        {
          <React.Fragment>
            <div className="container">
              <div className="p-grid p-fluid">
                <div className="p-col-12 p-md-4">
                  <InputText
                    placeholder="Name "
                    type="text"
                    size={30}
                    onChange={(event) => {
                      let userModel = this.state.userModel;
                      userModel.name = event.target.value;
                      this.setState({ userModel: userModel });
                    }}
                  />{" "}
                  {this.state.errors.name !== "" ? (
                    <Message severity="warn" text={this.state.errors.name} />
                  ) : null}
                </div>
                <br></br>
                <div className="p-col-12 p-md-4">
                  <InputText
                    placeholder="Email"
                    type="email"
                    onKeyPress={this.handleKeyPress}
                    size={30}
                    onChange={(event) => {
                      let userModel = this.state.userModel;
                      userModel.email = event.target.value;
                      this.setState({ userModel: userModel });
                    }}
                  />{" "}
                  {this.state.errors.email !== "" ? (
                    <Message severity="warn" text={this.state.errors.email} />
                  ) : null}
                </div>
                <br></br>
                <div className="p-col-12 p-md-4">
                  <InputText
                    placeholder="Password"
                    type="password"
                    onKeyPress={this.handleKeyPress}
                    size={30}
                    onChange={(event) => {
                      let userModel = this.state.userModel;
                      userModel.password = event.target.value;
                      this.setState({ userModel: userModel });
                    }}
                  />
                  {this.state.errors.password !== "" ? (
                    <Message
                      severity="warn"
                      text={this.state.errors.password}
                    />
                  ) : null}
                </div>
                <br></br>
                <Button label="Register" onClick={() => this.submit()} />
                {this.state.isValid === true
                  ? this.props.history.push({
                      pathname: "/login",
                      user: this.state.userModel,
                    })
                  : null}
              </div>
            </div>
            .
          </React.Fragment>
        }
      </React.Fragment>
    );
  }
}
