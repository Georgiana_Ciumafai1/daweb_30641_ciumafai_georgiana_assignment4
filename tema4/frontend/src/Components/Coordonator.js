import * as React from "react";
import signup from "../img/img-sl.png";
import "../css/Coordonator.css";
import { Grid, Cell } from "react-mdl";
function Coordonator() {
  return (
    <div style={{ width: "100%", margin: "auto" }}>
      {" "}
      <Grid className="landing-grid">
        <Cell col={12}>
          <img src={signup} alt="signup" className="avatar-img" />

          <div className="banner-text">
            <h1>Dr. Ing.Radu Razvan Slavescu</h1>
            <hr />

            <p>
              Machine learning | Natural language processing | Deep Learning{" "}
            </p>

            <div className="social-links">
              {/* Github */}
              <a
                href="https://scholar.google.ro/citations?user=cqZSwFoAAAAJ"
                rel="noopener noreferrer"
                target="_blank"
              >
                <i className="fa fa-google" aria-hidden="true" />
              </a>
            </div>
          </div>
        </Cell>
      </Grid>
    </div>
  );
}

export default Coordonator;
