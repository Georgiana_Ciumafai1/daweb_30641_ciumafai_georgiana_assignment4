import * as React from "react";
import "../../src/css/Footer.css";

export class Footer extends React.Component {
  render() {
    return (
      <div className="footer-style">
        <footer>&copy; Ciumafai Georgiana 2020</footer>
      </div>
    );
  }
}
