import React from "react";
import ReactDOM from "react-dom";
import Harta from "./Harta";
import Enzyme from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import { shallow } from "enzyme";

Enzyme.configure({ adapter: new Adapter() });
describe("Harta", () => {
  it("should show map", () => {
    const foo = true;
    expect(foo).toBe(true);
  });
});
it("renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(<Harta />, div);
});
it("renders map ok", () => {
  const wrapper = shallow(<Harta />);
  const har = wrapper.find("div div");
  expect(
    wrapper.findWhere((node) => node.prop("data-testid") === "map")
  ).toHaveLength(1);
});
