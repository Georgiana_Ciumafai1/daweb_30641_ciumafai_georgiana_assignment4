import Enzyme from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import { shallow } from "enzyme";
import React from "react";
import About from "./About";
import { mount } from "enzyme";
import { createMemoryHistory } from "history";

Enzyme.configure({ adapter: new Adapter() });

const history = createMemoryHistory();
history.push("/about");

it("button works ok", () => {
  const wrapper = shallow(<About />);
  expect(
    wrapper.findWhere((node) => node.prop("data-testid2") === "button")
  ).toHaveLength(1);
});
beforeEach(() => {});
describe("ChartClass.js", () => {
  it("Simulate click", () => {
    const wrapper = mount(<About history={history} />);
    const button = wrapper.find("Button").at(1).simulate("click");
  });
});
