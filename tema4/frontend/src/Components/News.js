import React, { Component } from "react";
import YouTube from "react-youtube";
import { useTranslation } from "react-i18next";

import { Grid, Cell } from "react-mdl";
function MyComponent() {
  const { t } = useTranslation();
  return <p>{t(localStorage.getItem("nw1"))}</p>;
}
function MyComponent2() {
  const { t } = useTranslation();
  return <p>{t(localStorage.getItem("nw2"))}</p>;
}
function MyComponent3() {
  const { t } = useTranslation();
  return <p>{t(localStorage.getItem("nw3"))}</p>;
}

class News extends Component {
  //const { t } = useTranslation();

  componentDidMount() {
    var pathxml = process.env.PUBLIC_URL + "news.xml";

    var parseString = require("react-native-xml2js").parseString;

    console.log(pathxml);
    fetch(pathxml)
      .then((response) => response.text())
      .then((response) => {
        parseString(response, function (err, result) {
          console.log(result);
          console.log(result.news.nw[0]);
          console.log(result.news.nw[1]);
          console.log(result.news.nw[2]);
          localStorage.setItem("nw1", result.news.nw[0].trim());
          localStorage.setItem("nw2", result.news.nw[1].trim());
          localStorage.setItem("nw3", result.news.nw[2].trim());
        });
      })
      .catch((err) => {
        console.log("fetch", err);
      });
  }

  render() {
    return (
      <>
        <div style={{ width: "100%", margin: "auto" }}>
          <Grid className="landing-grid">
            <Cell col={2}>
              <div className="banner-text">
                <MyComponent />
              </div>
            </Cell>
            <Cell col={2}>
              <div className="banner-text">
                <MyComponent2 />
              </div>
            </Cell>
            <Cell col={2}>
              <div className="banner-text">
                <MyComponent3 />
              </div>
              <br></br>
            </Cell>
          </Grid>
        </div>
        <br></br>
        <YouTube videoId="KgPSREMmA_0" />
        <br></br>
        <br></br>
      </>
    );
  }
}

export default News;
