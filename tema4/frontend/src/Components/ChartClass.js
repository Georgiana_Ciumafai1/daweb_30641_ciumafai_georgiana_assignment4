import * as React from "react";

import axios from "axios";
import "../css/ChartClass.css";
import { Chart } from "primereact/chart";
import { Card } from "primereact/card";

class ChartClass extends React.Component {
  constructor() {
    super();
    this.state = {
      commModel: {
        value: "",
      },
      commuri: [],
      dates: [],
      value13: 0,
      value14: 0,
      value15: 0,
      value16: 0,
      value17: 0,
      value18: 0,
      value19: 0,
      value20: 0,
      value21: 0,
      value22: 0,
      value23: 0,
      value27: 0,
    };
  }

  submit2 = () => {
    this.props.history.push(`/chart`);
  };
  componentDidMount() {
    axios
      .get("http://localhost:8000/api/getComment", {
        headers: { "Content-Type": "application/json" },
        withCredentials: true,
      })
      .then((response) => {
        this.setState({ commuri: response.data });

        var zi13 = 0;
        var zi14 = 0;
        var zi15 = 0;
        var zi23 = 0;
        var zi27 = 0;
        var zi28 = 0;
        var zi29 = 0;
        var zi30 = 0;
        var zi31 = 0;

        var arrayLength = this.state.commuri.length;
        for (var i = 0; i < arrayLength; i++) {
          var str = this.state.commuri[i].created_at;
          var str2 = str.substr(0, str.indexOf(" "));
          console.log(str2);
          if (str2 === "2020-05-13") {
            zi13 = zi13 + 1;
          }
          if (str2 === "2020-05-14") {
            zi14 = zi14 + 1;
          }
          if (str2 === "2020-05-15") {
            zi15 = zi15 + 1;
          }
          if (str2 === "2020-05-23") {
            zi23 = zi23 + 1;
          }
          if (str2 === "2020-05-27") {
            zi27 = zi27 + 1;
          }
          if (str2 === "2020-05-28") {
            zi28 = zi28 + 1;
          }
          if (str2 === "2020-05-29") {
            zi29 = zi29 + 1;
          }
          if (str2 === "2020-05-30") {
            zi30 = zi30 + 1;
          }
          if (str2 === "2020-05-31") {
            zi31 = zi31 + 1;
          }
        }
        this.setState({ value13: zi13 });
        this.setState({ value14: zi14 });
        this.setState({ value15: zi15 });
        this.setState({ value23: zi23 });
        this.setState({ value27: zi27 });
        this.setState({ value28: zi28 });
        this.setState({ value29: zi29 });
        this.setState({ value30: zi30 });
        this.setState({ value31: zi31 });
        console.log("succes aducere comms");
        console.log(this.state.value13);
      })
      .catch(() => {
        console.log("Eroare");
      });
  }
  render() {
    const data = {
      labels: [
        "13 Mai",
        "14 Mai",
        "15 Mai",
        "16 Mai",
        "17 Mai",
        "18 Mai",
        "19 Mai",
        "20 Mai",
        "21 Mai",
        "22 Mai",
        "23 Mai",
        "24 Mai",
        "25 Mai",
        "26 Mai",
        "27 Mai",
        "28 Mai",
        "29 Mai",
        "30 Mai",
        "31 Mai",
      ],
      datasets: [
        {
          type: "line",
          label: "Nr comments",
          borderColor: "#2196F3",
          borderWidth: 2,
          fill: false,
          data: [
            this.state.value13,
            this.state.value14,
            this.state.value15,
            this.state.value16,
            this.state.value17,
            this.state.value18,
            this.state.value19,
            this.state.value20,
            this.state.value21,
            this.state.value22,
            this.state.value23,
            this.state.value20,
            this.state.value20,
            this.state.value20,
            this.state.value27,
            this.state.value28,
            this.state.value29,
            this.state.value30,
            this.state.value31,
          ],
        },
      ],
    };
    return (
      <div>
        <br />

        <Card>
          <br />
          <div className="content-section introduction">
            <div className="feature-intro">
              <br />
              <h1> Chart based on number of comments in each day</h1>
            </div>
          </div>

          <div className="content-section implementation">
            <Chart type="bar" data={data} />
          </div>
        </Card>
      </div>
    );
  }
}
export default ChartClass;
