import React from "react";
import "./App.css";
import "primereact/resources/primereact.min.css";
import "primeicons/primeicons.css";
import "primereact/resources/themes/nova-light/theme.css";
import "font-awesome/css/font-awesome.css";
import Header from "./Components/Header";
import { Footer } from "./Components/Footer";
import Home from "./Components/Home";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import About from "./Components/About";
import { Nav } from "./Components/Nav";
import News from "./Components/News";
import Student from "./Components/Student";
import Coordonator from "./Components/Coordonator";
import Contact from "./Components/Contact";
import Language from "./Components/Language";
import { Login } from "./Components/Login";
import { Register } from "./Components/Register";
import Profile from "./Components/Profile";
import ChartClass from "./Components/ChartClass";
import Harta from "./Components/Harta";
function App() {
  return (
    <Router>
      <div className="App">
        <Header />
        <Nav />
        <Switch>
          <Route path="/" exact={true} component={Home} />
          <Route path="/home" exact={true} component={Home} />
          <Route path="/about" exact={true} component={About} />
          <Route path="/news" exact={true} component={News} />
          <Route path="/student" exact={true} component={Student} />
          <Route path="/coordonator" exact={true} component={Coordonator} />
          <Route path="/contact" exact={true} component={Contact} />
          <Route path="/language" exact={true} component={Language} />
          <Route path="/login" exact={true} component={Login} />
          <Route path="/register" exact={true} component={Register} />
          <Route path="/profile" exact={true} component={Profile} />
          <Route path="/chart" exact={true} component={ChartClass} />
          <Route path="/map" exact={true} component={Harta} />
        </Switch>
        <Footer />
      </div>
    </Router>
  );
}

export default App;
